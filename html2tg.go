// Copyright 2017 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package html2tg

import (
	"bytes"
	"io"
	"regexp"
	"strings"

	"golang.org/x/net/html"
	"unicode"
)

const (
	space = 0x20

	stateSkipTags  = 0
	stateInsideTag = 1
	stateStopped   = 255
)

var markupCharsRE = regexp.MustCompile("([\\[*_`])")

type parser struct {
	state             byte
	z                 *html.Tokenizer
	buffer, tagBuffer *bytes.Buffer
	href, tagName     []byte
	zBuffered         []byte
	rest              []byte
	depth             int
	limit             int
}

func Convert(h string, limit ...int) string {
	res, _ := ConvertReader(strings.NewReader(h), limit...)
	return strings.TrimSpace(string(res))
}

func ConvertReader(r io.Reader, lim ...int) ([]byte, []byte) {
	var limit int
	text := make([]byte, 0)

	if len(lim) == 0 {
		limit = 4000
	} else {
		limit = lim[0]
	}

	z := html.NewTokenizer(r)
	p := parser{
		state:  stateSkipTags,
		z:      z,
		buffer: bytes.NewBuffer(text),
		rest:   z.Buffered(),
		depth:  0,
		limit:  limit,
	}

	for p.running() {
		p.processNextToken()
	}

	return p.buffer.Bytes(), p.rest
}

func (p *parser) processNextToken() {
	switch p.zBuffered = p.z.Buffered(); p.z.Next() {
	case html.ErrorToken:
		p.stop()
	case html.TextToken:
		switch p.state {
		case stateSkipTags:
			p.renderText(p.z.Text())
		case stateInsideTag:
			appendTextToBuffer(p.tagBuffer, p.z.Text())
		}
	case html.StartTagToken:
		tagName, hasAttrs := p.z.TagName()
		switch p.state {
		case stateSkipTags:
			switch string(tagName) {
			case "p", "br":
				p.buffer.WriteString("\n\n")
			case "b", "strong", "i", "em", "code":
				p.goInsideTag(tagName)
			case "a":
				var href []byte
				for hasAttrs {
					var k, v []byte
					k, v, hasAttrs = p.z.TagAttr()
					if string(k) == "href" {
						href = v
						break
					}
				}
				if href == nil {
					break
				}
				p.href = href
				p.goInsideTag(tagName)
			}
		case stateInsideTag:
			if bytes.Equal(p.tagName, tagName) {
				p.depth++
				break
			}

			if string(tagName) != "a" {
				break
			}

			var href []byte

			for hasAttrs {
				var k, v []byte
				k, v, hasAttrs = p.z.TagAttr()
				if string(k) == "href" {
					href = v
					break
				}
			}
			if href == nil {
				break
			}

			p.renderText(p.tagBuffer.Bytes())
			if !p.running() {
				break
			}

			p.href = href
			p.goInsideTag(tagName)
		}
	case html.EndTagToken:
		tagName, _ := p.z.TagName()
		switch p.state {
		case stateInsideTag:
			if !bytes.Equal(tagName, p.tagName) {
				break
			}

			p.depth--

			if p.depth > 0 {
				break
			}

			p.renderTag()
			p.state = stateSkipTags
		}
	case html.SelfClosingTagToken:
		tagName, _ := p.z.TagName()
		if string(tagName) != "br" {
			break
		}
		p.buffer.WriteString("\n\n")
	}
}

func (p *parser) running() bool {
	return p.state != stateStopped
}

func (p *parser) stop() {
	p.state = stateStopped
}

func (p *parser) goInsideTag(tn []byte) {
	p.tagName = tn
	p.tagBuffer = bytes.NewBuffer(make([]byte, 0))
	p.rest = p.zBuffered
	p.depth = 1
	p.state = stateInsideTag
}

func (p *parser) renderTag() {
	tagBuffer := bytes.TrimSpace(p.tagBuffer.Bytes())
	p.buffer.WriteString(" ")
	switch string(p.tagName) {
	case "a":
		p.buffer.WriteString("[")
		p.buffer.Write(tagBuffer)
		p.buffer.WriteString("](")
		p.buffer.Write(p.href)
		p.buffer.WriteString(")")
	case "b", "strong":
		p.buffer.WriteString("*")
		p.buffer.Write(tagBuffer)
		p.buffer.WriteString("*")
	case "i", "em":
		p.buffer.WriteString("_")
		p.buffer.Write(tagBuffer)
		p.buffer.WriteString("_")
	case "code":
		p.buffer.WriteString("`")
		p.buffer.Write(tagBuffer)
		p.buffer.WriteString("`")
	}
}

func (p *parser) renderText(text []byte) {
	appendTextToBuffer(p.buffer, text)
	if p.buffer.Len() > p.limit {
		lastSpacePos := bytes.LastIndexFunc(p.buffer.Bytes()[:p.limit], unicode.IsSpace)
		if lastSpacePos > 0 {
			p.buffer.Truncate(lastSpacePos)
		} else {
			p.buffer.Truncate(p.limit)
		}
		p.stop()
	}
}

func appendTextToBuffer(b *bytes.Buffer, text []byte) {
	if b.Len() > 0 && b.Bytes()[b.Len()-1:][0] != '\n' {
		b.WriteByte(space)
	}
	b.Write(escape(compactSpaces(text)))
}

func compactSpaces(b []byte) []byte {
	return bytes.TrimSpace(b)
}

func escape(b []byte) []byte {
	return markupCharsRE.ReplaceAll(b, []byte("\\$1"))
}
