// Copyright 2017 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package html2tg_test

import (
	"testing"

	"gitlab.com/alxrem/html2tg"
)

type converterTest struct {
	name     string
	input    string
	expected string
}

func TestRender(t *testing.T) {
	tests := []converterTest{
		{"strip tags", "aaa <tt>bbb</tt> ccc", "aaa bbb ccc"},
		{"render bold 1", "aaa <b>bbb</b> ccc", "aaa *bbb* ccc"},
		{"render bold 2", "aaa <strong>bbb</strong> ccc", "aaa *bbb* ccc"},
		{"render italic 1", "aaa <i>bbb</i> ccc", "aaa _bbb_ ccc"},
		{"render italic 2", "aaa <em>bbb</em> ccc", "aaa _bbb_ ccc"},
		{"render link", `aaa <a href="/link">caption</a> ccc`, "aaa [caption](/link) ccc"},
		{"render link without href", `aaa <a target="_blank">caption</a> ccc`, `aaa caption ccc`},
		{"ignore internal tags", `aaa <b>caption <tt>type</tt></b> ccc`, "aaa *caption type* ccc"},
		{"render nested tags", `aaa <b>caption <b>nested</b></b> ccc`, "aaa *caption nested* ccc"},
		{"render internal link", `aaa <b>bold <a href="/link">caption</a> text</b> ccc`, "aaa bold [caption](/link) text ccc"},
		{"render line break", "aaa<br>ccc", "aaa\n\nccc"},
		{"render inline code", "aaa <code>ccc</code> bbb", "aaa `ccc` bbb"},
		{"escape *", "aaa * bbb", "aaa \\* bbb"},
		{"escape _", "aaa _ bbb", "aaa \\_ bbb"},
		{"escape [", "aaa [ccc] bbb", "aaa \\[ccc] bbb"},
		{"escape `", "aaa `ccc` bbb", "aaa \\`ccc\\` bbb"},
		{"escape * keep markup", "aaa * <b>ccc</b> bbb", "aaa \\* *ccc* bbb"},
		{"escape * inside tag", "aaa <b>cc * dd</b> bbb", `aaa *cc \* dd* bbb`},
		{"render <br>", "aaa <br> bbb", "aaa\n\nbbb"},
		{"render <br/>", "aaa <br/> bbb", "aaa\n\nbbb"},
		{"render <hr>", "aaa <hr> bbb", "aaa bbb"},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			text := html2tg.Convert(test.input)
			if text == test.expected {
				return
			}
			t.Fatalf("Got %s, expected %s\n", text, test.expected)
		})
	}
}

func TestLimit(t *testing.T) {
	tests := []converterTest{
		{"trim to last space", "aaa bbb ccc", "aaa bbb"},
		{"trim to limit", "abcdefghijklm", "abcdefghij"},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			text := html2tg.Convert(test.input, 10)
			if text == test.expected {
				return
			}
			t.Fatalf("Got %s, expected %s\n", text, test.expected)
		})
	}
}
